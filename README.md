# Django Beginner (By Fazt)

<div align="center">
   <img src="./myapp/static/img/Django-Logo.png" alt="Django Logo" width="350px" />
</div>

Project of a web application written in Django

> Note: This project was created from a tutorial

## How to use

**How to install dependencies**

```sh
$ pip install -r requirements.txt
```

> Note: As a recommendation, create a virtual environment
>
> ```
> python3.10 -m venv env
> ```

**How to migrate the tables**

```sh
$ python manage.py migrate
```

**How to run the project**

```sh
$ python manage.py runserver
```