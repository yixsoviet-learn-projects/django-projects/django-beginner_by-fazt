from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import Project, Task
from django.shortcuts import get_object_or_404, render, redirect
from .forms import CreateTaskForm, CreateProjectForm


def indexView(request):
    title = "Welcome to Django Course!!"
    return render(request, "index.html", {"title": title})


def helloWorldView(request):
    return HttpResponse(f"<h1>Hello World</h1>")


def aboutView(request):
    username = "YixSoviet"
    return render(request, "about.html", {"username": username})


def helloUserView(request, username):
    username = username.capitalize()
    return HttpResponse(f"<h1>Hello {username}</h1>")


def projectsView(request):
    # projects = list(Project.objects.values())
    projects = Project.objects.all()
    return render(request, "projects/projects.html", {"projects": projects})


def tasksView(request):
    # task = get_object_or_404(Task, id=id)
    tasks = Task.objects.all()
    return render(request, "tasks/tasks.html", {"tasks": tasks})


def newTaskView(request):
    # title = request.POST.get("title")
    # description = request.POST.get("description")
    # if title != None and description != None:
    #     Task.objects.create(title=title, description=description, project_id=2)

    if request.method == "GET":
        return render(
            request, "tasks/create_task.html", {"newTaskForm": CreateTaskForm()}
        )
    else:
        Task.objects.create(
            title=request.POST["title"],
            description=request.POST["description"],
            project_id=2,
        )
        return redirect("tasks")


def newProjectView(request):
    if request.method == "GET":
        return render(
            request,
            "projects/create_project.html",
            {"newProjectForm": CreateProjectForm()},
        )
    else:
        Project.objects.create(name=request.POST["name"])
        return redirect("projects")


def projectDetailView(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = Task.objects.filter(project_id=id)
    return render(
        request, "projects/detail.html", {"project": project, "tasks": tasks}
    )
