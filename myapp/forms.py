from django import forms


class CreateTaskForm(forms.Form):
    title = forms.CharField(
        label="Task title",
        max_length=250,
        widget=forms.TextInput(attrs={"class": "input"}),
    )
    description = forms.CharField(
        label="Task description",
        required=False,
        widget=forms.Textarea(attrs={"class": "input"}),
    )


class CreateProjectForm(forms.Form):
    name = forms.CharField(label="Project name", max_length=250, widget=forms.TextInput(attrs={"class": "input"}))
