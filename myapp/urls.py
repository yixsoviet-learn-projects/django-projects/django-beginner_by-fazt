from django.urls import path, re_path
from . import views

urlpatterns = [
    path("", views.indexView, name="index"),
    path("about/", views.aboutView, name="about"),
    path("hello/", views.helloWorldView, name="hello-view"),
    path("hello/<str:username>", views.helloUserView, name="hello-user"),
    path("projects/", views.projectsView, name="projects"),
    path("projects/<int:id>", views.projectDetailView, name="project-detail"),
    path("tasks/", views.tasksView, name="tasks"),
    path("new-task/", views.newTaskView, name="new-task"),
    path("new-project/", views.newProjectView, name="new-project"),
]
